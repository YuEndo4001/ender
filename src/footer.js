import React from "react";

import "./css/common.css";
import "./css/footer.css";

export function Footer() {
  return(
    <footer>
      <small className="copy">Copyright &copy; 2021 team ender. All rights reserved.</small>
    </footer>
  );
} 