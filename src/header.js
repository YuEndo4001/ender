import React from "react";
import { Link } from "react-router-dom";

import "./css/common.css";
import "./css/header.css";

export function Header() {
  return (
    <header>
      <div className="header_contents">
        <h1 className="page_title"><Link to="/">TODO</Link></h1>

        <div className="global_navi">
          <span className="global_link"><Link to="/">home</Link></span>
          <span className="global_link"><Link to="/task">add</Link></span>
          <span className="global_link"><Link to="/finished">finished</Link></span>
          <span className="global_link"><Link to="/delete">delete</Link></span>
        </div>
      </div>
    </header>
  );
}