import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import "whatwg-fetch";

import { Header } from "./header.js";
import { Clock } from "./clock.js";
import { Footer } from "./footer.js";
import { Title } from "./title.js";
import "./css/common.css";
import "./css/taskPage.css";

class TaskPage_Comp extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: "",
      comment:"",
      deadline: "",
    };
  }


  render() {
    const handleChange = (e) => {
      this.setState({ [ e.target.name ]: e.target.value });
    }


    const createButtonClicked = () => {
      confirm("作成しますか？") && postSend();
    }


    const postSend = () => {
      const data = {
        title: this.state.title,
        comment: this.state.comment,
        deadline: this.state.deadline
      };

      const deadline = new Date(this.state.deadline);

      var errors = [];
      if (this.state.title === ""){
        errors.push('タイトルを入力してください。');
      } else if(this.state.title.length > 20) {
        errors.push("タイトルを20文字以内で入力してください。");
      }
      if (this.state.deadline === ""){
        errors.push('〆切入力してください。');
      } else if(new Date() > deadline) {
        errors.push("〆切には明日以降を設定してください。");
      }
      if (this.state.comment.length > 30){
        errors.push("メモは30文字以内で入力してください。");
      }

      if(errors.length != 0){
        var err_msg = "";
        for(let i = 0; i < errors.length; i++) {
            if(i == 0){
              err_msg = err_msg + errors[i];
            } else {
              err_msg = err_msg + "\n" + errors[i];
            }
        }
        alert(err_msg);
        return;

      } else {
        fetch("http://localhost:8080/ender/task", {
          method: "post",
          body: JSON.stringify(data)
        })
        .then((response) => {
          if (response.status === 200) {
            this.props.history.push("/");
          } else {
            alert("作成に失敗しました。");
          }
        })
        .catch((error) => {
          console.error(error);
        });
      }
    }


    return(
      <div className="container">
        <Header />

        <main>
          <Clock />
          <Title />
          <div className="content">
            <table className="task_form">
              <tbody>
                <tr className="form_item">
                  <td>
                    <label htmlFor="title">タスク</label>
                  </td>
                  <td>
                    <input type="text" name="title" id="title" placeholder="例: 宿題" onChange={ handleChange } />
                  </td>
                </tr>

                <tr className="annotation">
                  <td></td>
                  <td>
                    <p>※20文字以内で入力してください。</p>
                  </td>
                </tr>

                <tr className="form_item">
                  <td>
                    <label htmlFor="comment">メモ</label>
                  </td>
                  <td>
                    <input type="text" name="comment" id="comment" onChange={ handleChange } />
                  </td>
                </tr>

                <tr className="annotation">
                  <td></td>
                  <td>
                    <p>※コメントを入力する際は30文字以内で入力してください</p>
                  </td>
                </tr>

                <tr className="form_item">
                  <td>
                    <label htmlFor="deadline">期限</label>
                  </td>
                  <td>
                    <input type="date" name="deadline" id="deadline" onChange={ handleChange } />
                  </td>
                </tr>

                <tr className="annotation">
                  <td></td>
                  <td>
                    <p>※明日以降を入力してください。</p>
                  </td>
                </tr>
              </tbody>
            </table>

            <button className="create_button" onClick={ createButtonClicked }>作成</button>

          </div>
        </main>

        <Footer />
      </div>
    );
  }
}

TaskPage_Comp.propTypes = {
  history: PropTypes.object
}

export const TaskPage = withRouter(TaskPage_Comp);
