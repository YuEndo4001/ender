import React from "react";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import { TopPage } from "./topPage.js";
import { TaskPage } from "./taskPage.js";
import { FinishedPage } from "./finishedPage.js";
import { DeletePage } from "./deletePage.js";

class App extends React.Component {
  render() {
    return(
      <Router>
        <Switch>
          <Route exact path="/" component={ TopPage } />
          <Route path="/task" component={ TaskPage } />
          <Route path="/finished" component={ FinishedPage } />
          <Route exact path="/delete" component={ DeletePage } />
          <Redirect from="/top" to="/" />
          <Redirect from="/fin" to="/finished" />
          <Redirect from="/del" to="/delete" />
        </Switch>
      </Router>
    );
  }
}

export default App;
