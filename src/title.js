import React from "react";

import "./css/common.css";

export function Title() {
  var path = location.pathname ;
  var title = "";
  if (path == "/"){
    title = "未完了タスク";
  }else if (path == "/task"){
    title = "新規タスク";
  }else if (path == "/finished"){
    title = "完了済みタスク";
  }else if (path == "/delete"){
    title = "削除済みタスク";
  }
  return(
    <h2 name="pageTitle">
      {title}
    </h2>
  );
}
