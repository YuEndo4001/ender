import React from "react";

import { Header } from "./header.js";
import { Clock } from "./clock.js";
import { Task } from "./task.js";
import { Footer } from "./footer.js";
import { Title } from "./title.js";
import "./css/common.css";
import "./css/topPage.css";

export class TopPage extends React.Component {

  constructor(props) {
    super(props);
    this.state={
      tasks: [],
      taskId: "",
      memo: "",
    };
  }


  componentDidMount() {
    fetch("http://localhost:8080/ender/returnTasks", {
      method: "GET"
    })
    .then( response => {
      response.json().then( json => {
        this.setState({ tasks: json })
      })
    })
  }


  render() {
    const taskList = this.state.tasks.filter(task => !task.isFinished).map( task => {
      return (
        <Task key={ task.id } id={ task.id } title={ task.title } memo={ task.comment }
          deadline={ task.deadline } isFinished={ task.isFinished } isDeleted={ task.isDeleted } />
      );
    });


    return(
      <div className="container">
        <Header />

        <main>
          <Clock />
          <Title />
          <div className="content">
            <table border="1">
              <thead>
                <tr className="table_title">
                  <th className="title_label">タスク</th>
                  <th className="memo_label">メモ</th>
                  <th className="status_label">状態</th>
                  <th className="deadline_label">期日</th>
                  <th className="submit_label">完了</th>
                  <th>削除</th>
                </tr>
              </thead>
              <tbody>
                { taskList }
              </tbody>
            </table>
          </div>
        </main>

        <Footer />
      </div>
    );
  }
}
