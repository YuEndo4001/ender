import React from "react";

import "./css/clock.css";

export class Clock extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    };
  }


  tick() {
    this.setState({ date: new Date() });
  }


  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 1000);
  }


  componentWillUnmount() {
    clearInterval(this.timer);
  }


  render() {
    return (
      <h2 className="clock">現在の日時：{ this.state.date.toLocaleString() }</h2>
    );
  }
}