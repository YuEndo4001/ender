import React from "react";
import PropTypes from "prop-types";

import { Header } from "./header.js";
import { Clock } from "./clock.js";
import { Footer } from "./footer.js";
import { Title } from "./title.js";

import "./css/common.css";
import "./css/finishedPage.css";

export class FinishedPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      selectedTasks: [],
    };
  }


  componentDidMount() {
    fetch("http://localhost:8080/ender/returnTasks", {
      method: "GET",
    })
    .then( response => {
      response.json().then( json => {
        this.setState({ tasks: json });
      });
    })
  }


  render() {
    const doneButtonClicked = (e) => {
      confirm("完了を取り消しますか？") &&
      postDone(e);
    }

    
    const postDone = (e) => {
      const data = {
        taskId: e.target.value,
      };
      
      fetch ("http://localhost:8080/ender/doneTask", {
        method: "post",
        body: JSON.stringify(data),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("取消できませんでした。");
        }
        
        this.props.history.push("/fin");
      })
      .catch ( error => {
        console.error(error);
      });
    }

    
    const deleteButtonClicked = e => {
      confirm("削除しますか？") &&
      postDelete(e);
    }
    
    
    const postDelete = (e) => {
      const data = {
        taskId: e.target.value,
      }

      fetch ("http://localhost:8080/ender/deleteTask", {
        method: "post",
        body: JSON.stringify(data),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("削除できませんでした。");
        }
        
        this.props.history.push("/fin");
      })
      .catch ( error => {
        console.error(error);
      });
    }

    
    const boxCheck = e => {
      const taskId = e.target.value;
      const tasks = this.state.selectedTasks;
      
      if (tasks.includes(taskId)) {
        this.setState({ selectedTasks: tasks.filter(id => id !== taskId) });
      } else {
        this.setState({ selectedTasks: tasks.concat(taskId) });
      }
    }
    
    
    const multipleCancelButtonClicked = () => {
      if (this.state.selectedTasks.length === 0) {
        alert("完了を取り消すタスクを選択してください");
        return;
      }
      
      confirm("まとめて完了を取り消しますか？") &&
      postMultipleCancel();
    }

    
    const postMultipleCancel = () => {
      const data = {
        taskIds: this.state.selectedTasks,
      }

      fetch ("http://localhost:8080/ender/undoneTasks", {
        method: "post",
        body: JSON.stringify(data),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("削除できませんでした。");
        }

        this.props.history.push("/fin");
      })
      .catch ( error => {
        console.error(error);
      });
    }


    const multipleDeleteButtonClicked = () => {
      if (this.state.selectedTasks.length === 0) {
        alert("削除するタスクを選択してください");
        return;
      }

      confirm("まとめて削除しますか？") &&
      postMultipleDelete();
    }


    const postMultipleDelete = () => {
      const data = {
        taskIds: this.state.selectedTasks,
      }

      fetch ("http://localhost:8080/ender/deleteTasks", {
        method: "post",
        body: JSON.stringify(data),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("削除できませんでした。");
        }

        this.props.history.push("/fin");
      })
      .catch ( error => {
        console.error(error);
      });
    }


    const taskList = this.state.tasks.filter(task => task.isFinished && !task.isDeleted).map( task => {
      const deadline = new Date(task.deadline);
      const deadlineView = deadline.getFullYear()+ "/" + (deadline.getMonth() + 1)+ "/" + deadline.getDate();

      return (
        <tr className="finished" key={ task.id }>
          <td><label className="selectbox_label" htmlFor={ task.id }><input type="checkbox" value={ task.id } onChange={ boxCheck } id={ task.id } /></label></td>
          <td>{ task.title }</td>
          <td className="memo">{ task.comment }</td>
          <td>完了</td>
          <td className="deadline_view">{ deadlineView }</td>
          <td className="done_button_box">
            <button className="done_button" value={ task.id } onClick={ doneButtonClicked }>取消</button>
          </td>
          <td className="delete_button_box">
            <button className="delete_button" value={ task.id } onClick={ deleteButtonClicked }>削除</button>
          </td>
        </tr>
      );
    });


    return (
      <div className="container">
        <Header />

        <main>
          <Clock />
          <Title />
          <div className="content">
            <table border="1">
              <thead>
                <tr className="table_title">
                  <th className="select_label">選択</th>
                  <th className="title_label">タスク</th>
                  <th className="memo_label">メモ</th>
                  <th className="status_label">状態</th>
                  <th className="deadline_label">期日</th>
                  <th className="submit_label">完了取消</th>
                  <th>削除</th>
                </tr>
              </thead>
              <tbody>
                { taskList }
              </tbody>
            </table>

            <button className="multiple_button" onClick={ multipleCancelButtonClicked }>選択済みを完了取消</button>
            <button className="multiple_button" onClick={ multipleDeleteButtonClicked }>選択済みを削除</button>
          </div>
        </main>

        <Footer />
      </div>
    );
  }
}

FinishedPage.propTypes = {
  history: PropTypes.object,
}
