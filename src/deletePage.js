import React from "react";
import PropTypes from "prop-types";
import { Header } from "./header.js";
import { Footer } from "./footer.js";
import { Title } from "./title.js";
import "./css/common.css";
import "./css/deletePage.css";

export class DeletePage extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      tasks: [],
      taskId: "",
      deleteList: []
    };
  }

  componentDidMount()   {
    fetch("http://localhost:8080/ender/returnTasks", {
      method: "GET"
    })
    .then( response => {
      response.json().
      then( json => {
        this.setState({ tasks: json })
      })
    })
  }

  render() {
    const cancelButtonClicked = () => {
      if(this.state.deleteList.length == 0){
        alert("削除を取り消すタスクを選んでください");
        return;
      }else{
        confirm("削除を取り消しますか？") && postCancelDelete();
      }
    }
    const postCancelDelete = () => {
      fetch ("http://localhost:8080/ender/cancelDeleteTaskList", {
        method: "post",
        body: JSON.stringify(this.state.deleteList),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("削除の取り消しができませんでした。");
        }
        this.setState({deleteList:''});
        this.props.history.push("/del");
      })
      .catch ( error => {
        console.error(error);
      });
    }
    const deleteButtonClicked = () => {
      if(this.state.deleteList.length == 0){
        alert("完全に削除するタスクを選択してください");
        return;
      }else{
        confirm("完全に削除しますか？") && postDelete();
      }
    }

    const postDelete = () => {
      fetch ("http://localhost:8080/ender/deleteTaskList", {
        method: "post",
        body: JSON.stringify(this.state.deleteList),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("削除できませんでした。");
        }
        this.setState({deleteList:''});
        this.props.history.push("/del");
      })
      .catch ( error => {
        console.error(error);
      });
    }
    const handleChange = (e) => {
      if (this.state.deleteList.includes(e.target.value)) {
        var deleteListArray = this.state.deleteList;
        var deleteIndex = deleteListArray.indexOf(e.target.value)
        deleteListArray.splice(deleteIndex,1);
        this.setState({deleteList: deleteListArray});
      }else{
        this.setState({deleteList: this.state.deleteList.concat(e.target.value)});
      }
    }
    const allCheck = () =>{
      if(typeof document.check != 'undefined'){
        var rowDeleteList = [];
        this.setState({deleteList: [] });
        for (var i=0; i<document.check.length; i++){
          if( typeof document.check.[i].task == 'undefined'){
            rowDeleteList.push(document.check.task.value)
            document.check.task.checked = true;
          }else{
            var deleteId = document.check.[i].task.value;
            rowDeleteList.push(deleteId);
            document.check.[i].task.checked = true;
          }
        }
        this.setState({deleteList: [...rowDeleteList]});
      }
    };
    const removeCheck = () =>{
      if(this.state.deleteList.length != 0){
        for (var i=0; i<document.check.length; i++){
          if (typeof document.check.[i].task == 'undefined'){
            document.check.task.checked = false;
          }else{
            document.check.[i].task.checked = false;
          }
        }
        this.setState({deleteList: [] });
      }
    }
    const taskList = this.state.tasks.map( task => {
      if (task.isDeleted) {
        return (
          <tr className="deleted">
          <td className="delete_button_box">
            <form name="check">
              <label className="selectbox_label">
              <input type="checkbox" value={task.id} onChange={handleChange} name="task" id={task.id}/>
              </label>
            </form>
          </td>
          <td>
            <label htmlFor={task.id}>
              { task.title }
            </label>
          </td>
          <td>
            <label htmlFor={task.id}>
              {task.comment}
            </label>
          </td>
          </tr>
        );
      }
    });

    return(
      <div className="container">
        <Header />
        <main>
          <Title />
          <div className="content">
            <table border="1">
              <thead>
                <tr className="table_title">
                <th className="select_label">選択</th>
                <th className="title_label">タスク名</th>
                <th className="memo_label">メモ</th>
                </tr>
              </thead>
              <tbody>
                { taskList }
              </tbody>
            </table>
            <button className="allCheck_button" onClick={ allCheck }>すべて選択</button>
            <button className="removeCheck_button" onClick={ removeCheck }>すべてのチェックを外す</button>
            <button className="physical_delete_button" name="taskId" onClick={ deleteButtonClicked }>削除</button>
            <button className="cancel_delete_button" name="taskId" onClick={ cancelButtonClicked }>削除を取り消す</button>
          </div>
        </main>

        <Footer />
      </div>
    );
  }
}

DeletePage.propTypes = {
  history: PropTypes.object
}
