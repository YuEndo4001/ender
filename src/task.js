import React from "react";
import PropTypes from "prop-types";

import "./css/common.css";
import "./css/task.css";

export class Task extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      status: "",
      memo: this.props.memo,
      deadline: this.props.deadline,
      isFinished: this.props.isFinished,
      isDeleted: this.props.isDeleted
    };
  }


  componentDidMount() {
    if (this.props.isFinished) {
      this.setState({ status: "完了" });
    } else if (!this.props.isFinished && (new Date().toLocaleDateString() > new Date(this.state.deadline).toLocaleDateString())) {
      this.setState({ status: "期限切れ" });
    } else {
      this.setState({ status: "-" });
    }
  }

  render() {
    const deadline = new Date(this.state.deadline);
    const deadlineView = deadline.getFullYear()+ "/" + (deadline.getMonth() + 1)+ "/" + deadline.getDate();


    const doneButtonClicked = () => {
      confirm(this.state.isFinished ? "完了を取り消しますか？" : "タスクを完了しますか？") &&
      postDone();
    }

    const postDone = () => {
      const data = {
        taskId: this.props.id,
      };

      fetch ("http://localhost:8080/ender/doneTask", {
        method: "post",
        body: JSON.stringify(data),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("完了できませんでした。");
        }

        this.setState({ isFinished: !this.state.isFinished });

        if (this.state.isFinished) {
          this.setState({ status: "完了" });
        } else if (!this.state.isFinished && (new Date().toLocaleDateString() > deadline.toLocaleDateString())) {
          this.setState({ status: "期限切れ" });
        } else {
          this.setState({ status: "-" });
        }
      })
      .catch ( error => {
        console.error(error);
      });
    }

    const deleteButtonClicked = e => {
      confirm("タスクを削除しますか？") && postDelete(e);
    }

    const handleChange = (e) => {
      this.setState({memo: e.target.value})
    }

    const settingButtonClicked = e => {
      confirm("コメントを編集しますか？") && postSetting(e);
    }

    const postSetting = () => {
      const data = {
        taskId: this.props.id,
        memo: this.state.memo,
      }
      if (this.state.memo.length > 30){
        alert("メモは30文字以内で入力してください。");
        return;
      }
      fetch ("http://localhost:8080/ender/settingMemo", {
        method: "post",
        body: JSON.stringify(data),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("編集できませんでした。");
        }
      })
      .catch ( error => {
        console.error(error);
      });
      nativeForm(this.props.id);      
      this.setState({ memo: this.state.memo });
    }

    var activeForm = (id) => {
      var closeId = "close" + id;
      var openId = "open" + id;
      document.getElementById(closeId).style.display = "none";
      document.getElementById(openId).style.display = "flex";
    }

    var nativeForm = (id) => {
      var closeId = "close" + id;
      var openId = "open" + id;
      document.getElementById(closeId).style.display = "flex";
      document.getElementById(openId).style.display = "none";
      this.setState({ memo: this.props.memo });
    }

    const postDelete = () => {
      const data = {
        taskId: this.props.id,
      }

      fetch ("http://localhost:8080/ender/deleteTask", {
        method: "post",
        body: JSON.stringify(data),
      })
      .then ( response => {
        if (response.status !== 200) {
          alert("削除できませんでした。");
        }

        this.setState({ isDeleted: !this.state.isDeleted });
      })
      .catch ( error => {
        console.error(error);
      });
    }


    return (
      <tr className={ (this.state.status === "完了" ? "hidden" : this.state.status === "-" ? "new" : "expired") + " " +
        (this.state.isDeleted ? "hidden" : "") }>
        <td>{ this.props.title }</td>
        <td className="memo" id={ "close" + this.props.id }>
          <div className="memo_box">
            {this.state.memo}
          </div>
          <div className="close_form_box">
            <button className="change_bottun" onClick={() => activeForm(this.props.id)}>編集</button>
          </div>
        </td>

        <td className="memo" id={ "open" + this.props.id }>
          <div className="memo_form_box">
            <input type="text" className="memo_setting_form" name="memo" id="memo" value={ this.state.memo } onChange={handleChange} />
          </div>
          <div className="setting_form_box">
            <button className="setting_bottun" onClick={ settingButtonClicked }>更新</button>
          </div>
          <div className="open_form_box">
            <button className="change_bottun" onClick={ () => nativeForm(this.props.id) }>閉じる</button>
          </div>
        </td>
        <td className={ this.state.status === "期限切れ" ? "deadline_over" : "" }>{ this.state.status }</td>
        <td className="deadline_view">{ deadlineView }</td>
        <td className="done_button_box">
          <button className="done_button" name="taskId" value={ this.props.id } onClick={ doneButtonClicked }>
            { this.state.isFinished ? "取消" : "完了" }
          </button>
        </td>
        <td className="delete_button_box">
          <button className="delete_button" name="taskId" value={ this.props.id } onClick={ deleteButtonClicked }>削除</button>
        </td>
      </tr>
    );
  }
}

Task.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  memo: PropTypes.string,
  deadline: PropTypes.number,
  isFinished: PropTypes.bool,
  isDeleted: PropTypes.bool,
  history: PropTypes.object,
}
