package com.example.demo;

import java.text.ParseException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("ender")
@RequiredArgsConstructor
public class SettingMemo {

	private final TaskService taskService;

	@CrossOrigin
	@PostMapping(value = "/settingMemo")
	public Task settingMemo(@RequestBody String json)
			throws JsonMappingException, JsonProcessingException, ParseException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);
		String comment = node.get("memo").textValue();
		Integer taskId = node.get("taskId").asInt();

		Task task = taskService.findTask(taskId);
		task.setComment(comment);

		return taskService.saveTask(task);
	}
}