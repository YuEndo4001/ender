package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("ender")
@RequiredArgsConstructor
public class DoneTask {

	private final TaskService taskService;

	@CrossOrigin
	@PostMapping(value = "/doneTask")
	public Task doneTask(@RequestBody String json)
			throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);

		Integer taskId = node.get("taskId").asInt();
		Task task = taskService.findTask(taskId);

		task.setIsFinished(!task.getIsFinished());

		return taskService.saveTask(task);
	}


	@CrossOrigin
	@PostMapping(value = "/undoneTasks")
	public void undoneTasks(@RequestBody String json)
			throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode nodes = mapper.readTree(json);
		List<Integer> taskIds = new ArrayList<>();

		for (JsonNode node : nodes.get("taskIds")) {
			taskIds.add(node.asInt());
		}

		for (Integer taskId : taskIds) {
			taskService.undoneTask(taskId);
		}
	}
}
