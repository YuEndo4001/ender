package com.example.demo;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("ender")
@RequiredArgsConstructor
public class ReturnTasks {

	private final TaskService taskService;

	@CrossOrigin
	@GetMapping(value = "/returnTasks")
	public ResponseEntity<String> returnTasks()
			throws JsonProcessingException {

		List<Task> taskList = taskService.findAllTasks();

		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(taskList);

		return ResponseEntity.ok()
						.header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString())
						.body(jsonStr);
	}
}
