package com.example.demo;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/ender")
@RequiredArgsConstructor
public class CreateTask {

	private final TaskService taskService;

	@CrossOrigin
	@PostMapping(value = "/task")
	public Task createTask(@RequestBody String json)
			throws JsonMappingException, JsonProcessingException, ParseException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);

		String title = node.get("title").textValue();
		String comment = node.get("comment").textValue();
		String deadline = node.get("deadline").textValue();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		Task task = new Task();

		task.setTitle(title);
		task.setComment(comment);
		task.setDeadline(format.parse(deadline));
//		task.setDeadline(Timestamp.valueOf(deadline + " 00:00:00"));
		task.setIsFinished(false);
		task.setIsDeleted(false);

		return taskService.saveTask(task);
	}
}
