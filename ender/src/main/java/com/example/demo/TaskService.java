package com.example.demo;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TaskService {

	private final TaskRepository taskRepository;

	public List<Task> findAllTasks(){
		return taskRepository.findAll(Sort.by(Sort.Direction.ASC, "deadline")
						.and(Sort.by(Sort.Direction.ASC, "id")));
	}

	public Task findTask(Integer id) {
		return taskRepository.getOne(id);
	}

	public Task saveTask(Task task) {
		return taskRepository.save(task);
	}

	public Integer undoneTask(Integer id) {
		return taskRepository.undoneById(id);
	}

	public Integer logicallyDeleteById(Integer id) {
		return taskRepository.logicallyDeleteById(id);
	}

	public void deleteTask(Integer id) {
		taskRepository.deleteById(id);
	}


//	public Integer updateTask(Integer id){
//		return taskRepository.saveById(id);
//	}
}
