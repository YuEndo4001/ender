package com.example.demo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("ender")
@RequiredArgsConstructor
public class DeleteTask {

	private final TaskService taskService;

	@CrossOrigin
	@PostMapping(value = "/deleteTask")
	public Task deleteTask(@RequestBody String json)
			throws JsonMappingException, JsonProcessingException, ParseException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(json);

		Integer taskId = node.get("taskId").asInt();
		Task task = taskService.findTask(taskId);

		task.setIsDeleted(!task.getIsDeleted());

		return taskService.saveTask(task);
	}


	@CrossOrigin
	@PostMapping(value = "/deleteTasks")
	public void deleteTasks(@RequestBody String json)
			throws JsonMappingException, JsonProcessingException, ParseException {

		ObjectMapper mapper = new ObjectMapper();
		JsonNode nodes = mapper.readTree(json);
		List<Integer> taskIds = new ArrayList<>();

		for (JsonNode node : nodes.get("taskIds")) {
			taskIds.add(node.asInt());
		}

		for (Integer taskId : taskIds) {
			taskService.logicallyDeleteById(taskId);
		}
	}


	@CrossOrigin
	@PostMapping(value = "/deleteTaskList")
	public void deleteTaskList(@RequestBody String json)
			throws JsonMappingException, JsonProcessingException, ParseException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(json);
		List<Integer> deleteList = new ArrayList<>();
		for (JsonNode node : jsonNode) {
		    deleteList.add(Integer.parseInt(node.asText()));
		}
		for (Integer deleteId : deleteList) {
			taskService.deleteTask(deleteId);
		}
	}

	@CrossOrigin
	@PostMapping(value = "/cancelDeleteTaskList")
	public void cancelDeleteTaskList(@RequestBody String json)
			throws JsonMappingException, JsonProcessingException, ParseException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(json);
		List<Integer> cancelDeleteList = new ArrayList<>();
		for (JsonNode node : jsonNode) {
		    cancelDeleteList.add(Integer.parseInt(node.asText()));
		}
		for (Integer cancelDeleteId : cancelDeleteList) {
			Task task = taskService.findTask(cancelDeleteId);

			task.setIsDeleted(!task.getIsDeleted());

			taskService.saveTask(task);
		}
	}
}
