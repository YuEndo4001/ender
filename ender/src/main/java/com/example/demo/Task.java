package com.example.demo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Entity
@Table(name = "tasks")
@Configuration
@Data
public class Task {
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String title;

	@Column(name = "is_finished")
	private Boolean isFinished;

	@Column(name = "is_deleted")
	private Boolean isDeleted;

	@Column
	private Date deadline;

	@Column
	private String comment;

	@Column
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	private Date createdDate;

	@Column
	@UpdateTimestamp
	@Temporal(TemporalType.DATE)
	private Date updatedDate;
}
