package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface TaskRepository extends JpaRepository<Task, Integer> {

	@Query("update Task t SET t.isFinished = false where t.id = :id")
	@Modifying
//	    @Modifying(clearAutomatically = false)
	Integer undoneById(@Param("id") Integer id);

	@Query("update Task t SET t.isDeleted = true where t.id = :id")
	@Modifying
//	    @Modifying(clearAutomatically = false)
	Integer logicallyDeleteById(@Param("id") Integer id);
}
